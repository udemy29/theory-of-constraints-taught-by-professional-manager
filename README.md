---
marp: true
theme: my-theme
headingDivider: 3
size: 16:9
paginate: true
---

# プロマネが教える制約理論<!-- omit in toc -->

<!-- _class: title -->

## 目次<!-- omit in toc -->

- [TOCの基礎](#tocの基礎)
- [継続改善のための集中の5ステップ](#継続改善のための集中の5ステップ)

## TOCの基礎

<!-- _class: subsection2 -->

### TOC（Theory Of Constraints）とは？

TOCとは、**全体最適**を実現するために、**制約**に集中してマネジメントすることで、
解決策を導く理論体系。

**システム**には、**つながり**と**ばらつき**があり、これらがあると**制約**がある。

この制約に集中することで、**全体に成果をもたらす**という理論。

### システムとは

- コンピューターシステムのことでない
- **相互に影響を及ぼしあう要素**から構成される、
**まとまりや仕組み全体**
- ビジネスでいえば**会社や組織などのまとまり**

### つながりとばらつき

<div class="left">

#### つながり

要素と要素の依存関係のこと。

<div class="mermaid">
flowchart LR
    A[作業A]
    B[作業B]
    C[作業C]
    A --> B --> C
</div>

</div>

<div class="right">

#### ばらつき

要素と要素の能力差のこと。

<div class="mermaid">
flowchart LR
    A[作業A: 1時間10件]
    B[作業B: 1時間8件]
    C[作業C: 1時間12件]
    A --> B --> C
</div>

</div>

### 制約

システムのパフォーマスンスを決定づける部分（**ボトルネック**）

<div class="mermaid">
flowchart LR
    A[作業A: 1時間10件]
    B[作業B: 1時間8件]
    C[作業C: 1時間12件]
    A --> B --> C
</div>

- 全体の成果は8件
- ボトルネックは作業B
- 作業Aや作業Cを効率化しても
**全体成果は変わらない**
- **作業Bを効率化する**

### TOCの基本的な3つの考え方

1. システムは**目的を持ち**、その目的を達成するために部分が存在する。
2. システムのパフォーマンスは、**一つまたは複数の制約によって決定**される
3. 全体（システム）は部分（要素）の**合計にはならない**

### TOCの基本的な3つの考え方の例

<div class="mermaid">
flowchart LR
    調達:80-->加工:70-->組立:50-->保管:150-->輸送:100-->販売:90-->利益:50
</div>

- システム：調達から販売までのサプライチェーン
- つながり：各工程の依存関係
- ばらつき：各工程の能力（調達：80個の製品を作るための部品を調達可能）
- システムの目的：利益を上げること
- 制約：組立工程
- 改善：組立工程を改善し、利益を上げる
- 制約に集中すれば大きな改善効果が得られる。
  - すべての工程の能力を10を上げる→利益＋10
  - 組立工程の能力を10を上げる→利益＋10

### TOCのポイント

- TOCを一言で言うなら、それは「**フォーカス（集中）**」だ
- 大事なのは、フォーカスするとは、**何をすべきか知っている**と同時に、
**何をすべきでないか知っている**ということだ
- なぜなら、**すべてにフォーカス**するのは、
**どれにもフォーカスしないのと同じ**だからだ

### 制約の種類

1. 物理制約
2. 市場制約
3. 方針制約

### 物理制約

- 物理的（ハード）な制約
- 装置や設備、人的リソース、またそれらの**能力不足に起因するもの**
- 生産工場で例えるなら「**生産能力＜市場の要求**」となっている状態
- 制約として認識することが割りと簡単
- 理論上の能力を100%発揮していることは少なく、大体段取り調整や手戻り、設備の故障などで、能力を発揮できていないことが多い
- 物理制約の根本的な原因を探ると、方針制約が引き金になっていることが多い

### 市場制約

- **企業の外（顧客や業界）の制約**
- 要するに供給能力はあるのに、需要自体が少ない状態
- 人や設備のあまりから、不要なものを作るなどで財務が悪化
- ITの現場で言えば不要な機能の開発（**不要なコード負債**）
- この場合は市場競争力の強化や、市場の拡大、新規市場の開拓を行う必要がある
- 市場制約も方針制約が根本原因である場合が多い

### 方針制約

- **会社や組織の方針や習慣による制約**
- 定められたルール、決まり、習慣、評価基準があるために「正しい行動」ができなかったり抑制されている状態
- 以前は正しかったが、現在では正しい行動を出来なくしたり、抑制したりする
- 生産現場で言えば、必要ないにも関わらず評価基準の稼働率を維持するために、注文がない製品を生産したりする
- 方針制約があると、正しい行動がわかっていてもその通りに行動できない
- 厄介なことに、それらの制約は常に「当たり前なもの」「正しいもの」として認識されているため、制約の存在に気づかないことも多い
- 企業文化や風土に起因することも多く、3つの制約の中で一番解決が難しい

### 「依存的事象」と「統計的変動」

- 「つながり」を言い換えれば「依存的事象」
- 「ばらつき」の考え方の中には「統計的変動」が含まれている
- 依存的事象
  - ある要因と別の要因が従属関係にある状態のこと
  - 作業工程の前後関係
- 統計的変動
  - 体調不良、事故、故障、災害など、確率的に発生する作業パフォーマンスの変動のこと
  - 通常回避できない

### 統計的変動の例

![統計的変動の例](./images/統計的変動の例.png)

- 各工程は平均60の能力がある
- しかし、目的である利益は60とならない
- どこかの工程で平均を下回る結果がでる
- そうするとそこが制約となり、後工程ではそれ以下の成果しかでない

## 継続改善のための集中の5ステップ
<!-- _class: subsection2 -->

### 集中の5ステップ

制約のうち主に**物理制約に対して有効**なフレームワーク。

1. 制約条件を特定する（Identify）
2. 制約を徹底活用する方法を決める（Exploit）
3. 上記の決定に全てを従属させる（Subordinate）
4. 制約の能力を高める（Elevate）
5. 惰性に注意しながら最初からくり返す（Beware of Inertia）

### ① 制約条件を特定する

- そのシステムにおける制約を特定する
- 業務のフローを最も遅くしている場所が制約である可能性が高い

<div class="mermaid">
flowchart LR
    調達:80 --> 加工:70 --> 組立:50 --> 保管:150 --> 輸送:100 --> 販売:90 --> 利益:50
</div>

- 組立が制約
- 制約条件を見つける際の視点
  - 在庫やタスクが溜まる
  - 処理時間や作業時間が長い
  - 問題が頻繁に発生する
  - 稼働率が高い

### ② 制約を徹底活用する方法を決める

- 制約を最大限活用する方法を決める
- まずは今の制約の潜在能力をどのように「**使い切るか**」を考える
- 新規に設備を投入したり、追加要因を入れたりする対策は、まだここではしない
- 例
  - 制約でしかできない仕事に専念できる方法を決める
  - 制約が待ち状態にならずに、常に稼働できる方法を決める
  - 制約が無駄な作業をしなくて済む方法を決める

### ③ 上記決定に全てを従属させる

- これまでの決定に非制約部分を従属させる
- とにかく制約が100%の力を発揮できるように周りが全力でサポートする
- **制約は「悪」ではなく、「貴重」なリソース**だと周囲が認識すること
![制約のサポート](./images/制約のサポート.png)

### ③ 上記決定に全てを従属させる（続き）

- 例
  - 制約以外でもできる作業は、非制約が引き取る
  - 制約に待ち時間ができないよう、処理できる内容をあらかじめ手前に積む
  - 他作業を止めてでも制約をサポートし、制約の作業が滞らないようにする
  - 制約の手前に品質確認のプロセスを移し、制約がやる作業が無駄にならないようにする
  - バッチサイズを小さくする

### ④ 制約の能力を高める

- ここでいよいよ設備の追加や人員追加を行う
- 能力を高める例
  - 制約の作業を担える設備や機械、要因を追加する
- ②③をやらずに④を行うのはNG
  - 能力を最大限発揮できない状態になるため
  - ④には大きなコストがかかることが多い

### ⑤ 惰性に注意しながら最初からくり返す

- これまでのステップにより、制約が別の場所に移っている可能性がある
- そのため、今まで進めてきた行動が崩れないようにしながら、新しい制約に対して同様のステップをくり返す
- これをくり返すと、だんだんと物理制約が市場制約や方針制約に移っていく
- そうなったら、次のセクションで紹介する「思考プロセス」のフレームワークを活用して、組織の変革を行う

## 変革のためのTOC思考プロセス
<!-- _class: subsection2 -->

### TOC思考プロセスの考え方

- 問題解決のためには、ステークホルダーとの協力や合意形成が必要不可欠
- TOC思考プロセスは**人間の心理的抵抗**や、その原因となっている**思い込みを取り除き**ながら、合**意形成をして問題を解決**する、体系的な問題解決アプローチ
- 従来の問題解決の思考プロセスとは考え方が異なる

### そもそも改善とは何か

- 改善とは「現状を目標に近づける」こと
- それはネガティブなことを取り除くことかもしれないし、
ポジティブなことを獲得しにいくことがもしれない
- 目標に近づくためには、今のやり方を変えなければいけない
- なぜなら、今までと同じことをしていては（外的要因がかわらない限り）今までと同じ結果しか手に入れられないから
- つまり改善とは、今のシステムに新しい解決策を導入した結果である

### 改善しようとすると何が起こるか

- 改善には変化が伴う
- 変化が必要なとき、変化させる側と変化させられる側が存在する
- このとき人は対立する

### なぜ変化に対して抵抗が起こるのか

<div class="left">

- 人は元来リスクを恐れる動物
- マズローの欲求5段解説でも安全欲求は生理的欲求の次にくるぐらい強い
- 人は変化をさけることで安全欲求を満たし生き残ってきた
- 改善のためにはこの抵抗による対立と向き合わなければならない。

</div>

<div class="right">

![マズローの5段階説](./images/マズローの5段階説.png)

</div>

### 思い込みとは？

<div class="left">

- 対立構造が生まれるときには、実は「思い込み」が潜んでいる
- 互いの立場を主張する際には、必ずその前提となる「仮説」がある
- あなたが「セールをやめるべき」と主張したとき、相手の「セールをやるべき」という考えにはどんな仮説が潜んでいるだろうか？お
- この仮説（思い込み）を取り除くアプローチをとることで、「セールをやる/やめる」という対立を解消

</div>

<div class="right">

![思い込みの例](images/思い込みの例.drawio.png)

</div>

### 一般的な問題解決思考プロセスとの違い

![center w:700](images/一般的な問題解決思考プロセスとの違い.drawio.png)

### TOC思考プロセスの全体像

| 3つの質問                                                   | 抵抗の6階層                                | 合意の6階層                                          | 思考プロセスツール                                  |
| ----------------------------------------------------------- | ------------------------------------------ | ---------------------------------------------------- | --------------------------------------------------- |
| 何を変えるのか<br/>What to Change                           | 問題の存在に合意しない                     | 問題が存在すると合意する                             | 現状構造ツリー<br/>CRT: Current Reality Tree"       |
| <br/>何に変えるのか<br/>What to Change to                   | 解決策の方向性に合意しない                 | 解決策の方向性に合意する                             | 対立解消ツリー<br/>CRD: Conflict Resolution Diagram |
|                                                             | 解決策が問題を解決できることに合意しない   | 解決策が問題を解決できることに合意する               | 未来構造ツリー<br/>FRT: Future Reality Tree"        |
| <br/>どのように変化を起こすのか<br/>How to Cause the Change | 解決策を実行するとネガティブな副作用がある | ネガティブな副作用を防げることに合意する             | 前提条件ツリー<br/>PRT: Prerequisite Tree           |
|                                                             | 解決策を実行するのに障害がある             | 障害を回避できることに合意する                       | 移行ツリー<br/>TrT: Transition Tree                 |
|                                                             | 結果起こる未知の問題や障害への恐れ         | 未知の問題や障害のリスクを取り実行することに合意する | 前述のツール全てを活用                              |

### 改善のための3つの質問

1. 何を変えるのか（What to Change）
    - 現在のシステムの状態を明らかにし、**目的を阻害している行動や考え方を探す**
    - 変えなければならない**本質的な問題（中核問題）を見つける**
2. 何に変えるのか（What to Change to）
    - 実現可能な将来の状況や**理想の状態を考える**
    - **中核問題に対する解決策を見つける**
    - また、解決策による**副作用と、その対策も検討**する
3. どのように変化を起こすか（How to Cause the Change）
    - 理想の状態にするために、必要な変化を起こすための**詳細なステップを考える**
    - また、解決策の**実行時に発生する様々な障害を洗い出し、それらを乗り越えるための方法を洗い出す**

### 抵抗の6階層

- 変化のためにはステークホルダの協力や合意が不可欠
- だが前述のとおり変化を起こす際には抵抗が起きる
- 変化への抵抗にあは6つの階層があり取り除く必要がある

### ① 問題の存在に合意しない

<div class="left">

- 問題提起に対して「問題など起きていない」「存在しない」という抵抗
- 問題がおきていないという「思い込み」を取り除き、問題自体の存在を認識してもらう必要がある
- 問題の構造を可視化して認識してもらう（現状構造ツリー）

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: 従来の改善方法には問題があります
    上司->>-あなた: 作業効率向上が重要だ！今のやり方に問題ない！
</div>

</div>

### ② 解決策の方向性に合意しない

<div class="left">

- 問題の存在は合意したが、解決策の方向性に抵抗する
- この解決策が良くないと考えた「思い込み」を取り除く
- なぜこの方向性で良いのかを紐解きながら説明する

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: TOCの考え方を導入して利益があがるようにしましょう
    上司->>-あなた: 従来のやり方と親和性がある方法がいいに決まっている！
    Note over あなた,上司: 新しいやり方なんて上手くいくわけがない。過去にも失敗した。失敗できない。
</div>

</div>

### ③ 解決策が問題解決するのを合意しない

<div class="left">

- 解決策の方向性には合意したが、この解決策では問題が解決できないのではないかという抵抗
- この解決策では問題が解決しないと考えた「思い込み」を取り除く
- 解決策によって問題が解決した未来を示す

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: ではこの検討をこのまま進めて。。。
    上司->>-あなた: 待て！方向性はわかったが、本当に問題解決する？
    Note over あなた,上司: 解決策は以前の失敗と違う。でも本当にこれで解決？
</div>

</div>

### ④ 解決策を実行すると副作用がある

<div class="left">

- 解決策を導入した後に起きる副作用（新しい問題）に対して抵抗する
- 副作用が回避できないという「思い込み」を取り除く
- 想定される副作用とそれに対する打ち手を示す

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: では解決策を実行。。。
    上司->>-あなた: いや！しかしこれを実行すると副作用（XXX）が起きる
    Note over あなた,上司: 良いことばかりのはずかない
</div>

</div>

### ⑤ 解決策を実行するのに障害がある

<div class="left">

- 解決策を導入する際に障害が起きるはずだとうい抵抗
- 障害が回避できないという「思い込み」を取り除く
- 障害は事前に克服できることを示す

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: では解決策を実行。。。
    上司->>-あなた: まて！これを実行すると課題が発生して解決策が頓挫するのでは？
    Note over あなた,上司: 万事うまく最後まで導入できるはずがない
</div>

</div>

### ⑥ 結果起こる未知の問題や障害への恐れ

<div class="left">

- 何が起きるかわからないという未知への恐怖からくる抵抗
- ここまでくると最後は感情
- 相手に対する傾聴や寄り添う姿勢、オーナーシップを示して合意を得る

</div>

<div class="right">

<div class="mermaid">
sequenceDiagram
    actor あなた
    actor 上司

    あなた->>+上司: では解決策を実行。。。
    上司->>-あなた: しかし、不安だ。。。本当に大丈夫？
    Note over あなた,上司: 前例ないし、何がおきるかわからない
</div>

</div>

### 思考プロセスのツール

提案合意形成が必要、仕事上の障害と対策を洗出したいなど様々な場面で利用できる。

| 種類                                               | 用途                             |
| -------------------------------------------------- | -------------------------------- |
| 現状構造ツリー（CRT: Current Reality Tree）        | 中核問題を特定                   |
| 対立解消ツリー（CRD: Conflict Resolution Diagram） | 対立を発見し解決                 |
| 未来構造ツリー（FRT: Future Reality Tree）         | 解決策が問題解決できることを確認 |
| 前提条件ツリー（PRT: Prerequisite Tree）           | 障害を克服する中間目標を検討     |
| 移行ツリー（TrT: Transition Tree）                 | 具体的な計画策定                 |

### 記号の凡例

![center](./images/記号の凡例.png)

### 専門用語

<div class="left">

- UDE(Undesirable Effect)※ウーディー
  - 目に見えて起きている問題事象であり事実
  - UDEの因果関係を辿ると中核問題にたどり着く
- DE（Desirable Effect）
  - UDEの裏返し
  - 問題が消えた後の望ましい状態
- 中核問題
  - 多くの問題の要因となっている根本的な原因

</div>

<div class="right">

- インジェクション
  - 解決策
  - 病気の患者に対して注射を打つイメージ
- 障害
  - 解決策を実行・導入中での障害
- ネガティブブランチ（副作用）
  - 解決策を導入したことで生じる新しい問題

</div>

### 現状構造ツリー

<div class="left">

- CRT: Current Reality Tree
- 3つの質問のうち、「何を変えるのか」を考える際に使う
- 問題の構造化を可視化して、変えるべき中核問題を明らかにする
- これを使えば、問題の存在を明らかにできる

</div>

<div class="right">

![center](./images/現状構造ツリー.png)

</div>

### 現状構造ツリーの作り方①

<div class="left">

1. まずはUDEを洗い出す
   - 組織の目標を阻害する、好ましくない状態を作り出しているUDEを洗い出す
2. UDEを絞り込む（10～20個目安）
   - 重要そうなUDEに絞り込む
   - なるべく複数の目線や立場で評価する
   - 何が重要かは立場や人によることを理解しなければならない

</div>

<div class="right">

| ID   | UDE（目に見える問題）                  |
| ---- | -------------------------------------- |
| UDE1 | プロジェクトが多すぎる                 |
| UDE2 | 会議が多すぎる                         |
| UDE3 | 自分のタスクに割くフリー時間がない     |
| UDE4 | 顧客の満足度が低下している             |
| UDE5 | 納期が遅れる                           |
| UDE6 | 顧客からのコストダウン要求が止まらない |

</div>

### 現状構造ツリーの作り方②

<div class="left">

3. 問題同士を因果関係でつなげる
   - UDEだけで関係が説明出来ない場合は必要な事象を追加する
   - ループも考える
   - 因果関係に論理の飛躍がないかを確認する
   - 原因を深掘りしたりMECEに考えるものではなく、**つながりから中核問題を見つける**
</div>
<div class="right">

![center w:400](./images/現状構造ツリーの作り方②.drawio.svg)

</div>

### 対立解消ツリー（CRD）

<div class="left">

- CRD: Conflict Resolution Diagram
- CRDには3つの質問のうち「何に変えるのか」を考える
- 中核問題を引き起こしている対立と仮定（思い込み）を明らかにする
- これを使えば、解決策の方向性に合意できる

</div>

<div class="right">

![対立解消ツリー](./images/対立解消ツリー.png)

</div>

### 対立解消ツリーの作り方①

<div class="left">

1. まずはハコを作る
  - Aは上段と下段両方の共通の目標
  - BとCはAを達成するための必要条件「～しなければならない」という要望
  - DとD'はその要望を実現するには「～することが前提となる」という前提条件の行動
  - またそれぞれつながりが成り立つための仮定

</div>

<div class="right">

![対立解消ツリー](./images/対立解消ツリー.png)

</div>

### 対立解消ツリーの作り方②

<div class="left">

2. Dに中核問題を引き起こしている行動を入れる
3. D'に対立する行動を入れる
4. BとCに要望を入れる
5. Aに上段と下段の共通目標を入れる
   - 抽象度が高い目標だと考えやすい
6. 仮定を入れる
7. 論理的に成り立っているか確認
8. 間違っている前提（思い込み）を見つける

</div>

<div class="right">

![center](images/対立解消ツリーの例.drawio.svg)

</div>
